using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcClient_Survey;
using Microsoft.Extensions.Logging;

namespace GrpcService_Survey
{
    public class CombinerService : Combiner.CombinerBase
    {
        private readonly ILogger<CombinerService> _logger;

        public CombinerService(ILogger<CombinerService> logger)
        {
            _logger = logger;
        }

        public override Task<AllPropertyReply> AllProperty(AllPropertyRequest request, ServerCallContext context)
        {
            return Task.FromResult(new AllPropertyReply
            {
                Alias =
                {
                    new List<AllPropertyReply.Types.EnumAllowingAlias>
                    {
                        AllPropertyReply.Types.EnumAllowingAlias.Started, AllPropertyReply.Types.EnumAllowingAlias.Unknown
                    }
                }
            });
        }

        public override async Task StreamingTest(IAsyncStreamReader<StreamingTestRequest> requestStream, IServerStreamWriter<StreamingTestReply> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext())
            {
                var currentDataFromClient = requestStream.Current.DataFromClient;
                Console.WriteLine(currentDataFromClient);

                await responseStream.WriteAsync(new StreamingTestReply { DataFromServer = $"Accept_{currentDataFromClient}"});
            }
        }
    }
}