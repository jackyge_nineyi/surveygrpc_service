﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace GrpcService_Survey.Middleware
{
    public class ViewGrpcMiddleware
    {
        private readonly RequestDelegate _next;

        public ViewGrpcMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string requestContent;

            using (var reader = new StreamReader(context.Request.Body))
            {
                requestContent = await reader.ReadToEndAsync();
                context.Request.Body.Seek(0, SeekOrigin.Begin);
            }

            // Call the next delegate/middleware in the pipeline
            await _next(context);
        }
    }
}